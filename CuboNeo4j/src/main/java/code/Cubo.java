package code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Cubo {

	// Posici�n de referencia
	private String posid;
	// Variable auxiliar
	private String posicionAuxiliar;
	// Lista de las posiciones de las 24 orientaciones posibles en formato String
	private List<String> posicionesFinal;
	// Lista de las posiciones de las 24 orientaciones posibles en formato
	// List<Integer>
	private List<List<Integer>> posicionesFinalesInteger;

	// Constructor
	public Cubo(List<List<Integer>> posiciones) {
		super();
		this.posicionesFinalesInteger = posiciones;

		this.posicionesFinal = new ArrayList<String>();
		this.posicionAuxiliar = "";

		// Para cada posici�n en formato List<Integer>, la convierto en un String y la
		// guardo en conjunto
		posiciones.stream().forEach(pos -> {
			pos.stream().forEach(i -> posicionAuxiliar += i.toString());
			posicionesFinal.add(posicionAuxiliar);
			posicionAuxiliar = "";
		});

		// Ordeno el conjunto
		Collections.sort(posicionesFinal, (p1, p2) -> p1.compareTo(p2));
		// Cojo la menor posici�n
		this.posid = posicionesFinal.get(0);
	}

	// GETS Y SETS
	public String getPosicionAuxiliar() {
		return posicionAuxiliar;
	}

	public void setPosicionAuxiliar(String posicionAuxiliar) {
		this.posicionAuxiliar = posicionAuxiliar;
	}

	public List<String> getPosicionesFinal() {
		return posicionesFinal;
	}

	public void setPosicionesFinal(List<String> posicionesFinal) {
		this.posicionesFinal = posicionesFinal;
	}

	public List<List<Integer>> getPosicionesFinalesInteger() {
		return posicionesFinalesInteger;
	}

	public void setPosicionesFinalesInteger(List<List<Integer>> posicionesFinalesInteger) {
		this.posicionesFinalesInteger = posicionesFinalesInteger;
	}

	public String getPosid() {
		return posid;
	}

	public void setPosid(String posid) {
		this.posid = posid;
	}

	// HASHCODE Y EQUALS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((posid == null) ? 0 : posid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cubo other = (Cubo) obj;
		if (posid == null) {
			if (other.posid != null)
				return false;
		} else if (!posid.equals(other.posid))
			return false;
		return true;
	}
}