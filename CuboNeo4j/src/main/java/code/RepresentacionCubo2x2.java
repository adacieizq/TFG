package code;

import java.util.ArrayList;
import java.util.List;

public class RepresentacionCubo2x2 {

	public List<Integer> verCubo(List<Integer> cubo) {
		List<Integer> res;
		if (cuboValido(cubo) == false) {
			res = null;
			System.out.println("Cubo no v�lido");
		} else {
			System.out.println("Cara Front:");
			System.out.println(cubo.get(0) + " " + cubo.get(1) + " \n" + cubo.get(2) + " " + cubo.get(3));
			System.out.println("Cara Right:");
			System.out.println(cubo.get(4) + " " + cubo.get(5) + " \n" + cubo.get(6) + " " + cubo.get(7));
			System.out.println("Cara Back:");
			System.out.println(cubo.get(8) + " " + cubo.get(9) + " \n" + cubo.get(10) + " " + cubo.get(11));
			System.out.println("Cara Left:");
			System.out.println(cubo.get(12) + " " + cubo.get(13) + " \n" + cubo.get(14) + " " + cubo.get(15));
			System.out.println("Cara Up:");
			System.out.println(cubo.get(16) + " " + cubo.get(17) + " \n" + cubo.get(18) + " " + cubo.get(19));
			System.out.println("Cara Down:");
			System.out.println(cubo.get(20) + " " + cubo.get(21) + " \n" + cubo.get(22) + " " + cubo.get(23) + "\n");
			System.out.println("========================================\n");
			res = cubo;
		}

		return res;
	}

	public boolean cuboValido(List<Integer> cubo) {
		boolean res;
		List<Integer> cero = new ArrayList<Integer>();
		List<Integer> uno = new ArrayList<Integer>();
		List<Integer> dos = new ArrayList<Integer>();
		List<Integer> tres = new ArrayList<Integer>();
		List<Integer> cuatro = new ArrayList<Integer>();
		List<Integer> cinco = new ArrayList<Integer>();

		for (Integer i : cubo) {
			if (i == 0) {
				cero.add(i);
			} else if (i == 1) {
				uno.add(i);
			} else if (i == 2) {
				dos.add(i);
			} else if (i == 3) {
				tres.add(i);
			} else if (i == 4) {
				cuatro.add(i);
			} else if (i == 5) {
				cinco.add(i);
			}
		}

		if (cubo.size() == 24 && cero.size() == 4 && uno.size() == 4 && dos.size() == 4 && tres.size() == 4
				&& cuatro.size() == 4 && cinco.size() == 4) {
			res = true;
		} else {
			res = false;
		}

		return res;
	}

	public String formatoCSV(List<Integer> cubo) {
		String res = "";
//		if (cuboValido(cubo) == false) {
//			res = null;
//			System.out.println("Cubo no v�lido");
//		} else {
			for (int i = 0; i < 24; ++i) {
				res = res + cubo.get(i).toString();
			}
//		}
		return res;
	}
}