package code;

import java.util.List;

public class MetodosResolucion {

	public String proximoMovimiento(List<Integer> cuboX, String posIdY) {
		MovimientosCubo2x2 movimientos = new MovimientosCubo2x2();
		String res = "";
		List<Integer> cuboXB = movimientos.movimientoB(cuboX);
		List<Integer> cuboXBi = movimientos.movimientoBi(cuboX);
		List<Integer> cuboXD = movimientos.movimientoD(cuboX);
		List<Integer> cuboXDi = movimientos.movimientoDi(cuboX);
		List<Integer> cuboXL = movimientos.movimientoL(cuboX);
		List<Integer> cuboXLi = movimientos.movimientoLi(cuboX);
		List<Integer> cuboXU = movimientos.movimientoU(cuboX);
		List<Integer> cuboXUi = movimientos.movimientoUi(cuboX);
		List<Integer> cuboXF = movimientos.movimientoF(cuboX);
		List<Integer> cuboXFi = movimientos.movimientoFi(cuboX);
		List<Integer> cuboXR = movimientos.movimientoR(cuboX);
		List<Integer> cuboXRi = movimientos.movimientoRi(cuboX);
		
		if(movimientos.crearCuboDesdePosicion(cuboXB).getPosid() == posIdY) {
			res = "B";
		} else if(movimientos.crearCuboDesdePosicion(cuboXBi).getPosid() == posIdY) {
			res = "BI";
		} else if(movimientos.crearCuboDesdePosicion(cuboXD).getPosid() == posIdY) {
			res = "D";
		} else if(movimientos.crearCuboDesdePosicion(cuboXDi).getPosid() == posIdY) {
			res = "DI";
		} else if(movimientos.crearCuboDesdePosicion(cuboXL).getPosid() == posIdY) {
			res = "L";
		} else if(movimientos.crearCuboDesdePosicion(cuboXLi).getPosid() == posIdY) {
			res = "LI";
		} else if(movimientos.crearCuboDesdePosicion(cuboXU).getPosid() == posIdY) {
			res = "U";
		} else if(movimientos.crearCuboDesdePosicion(cuboXUi).getPosid() == posIdY) {
			res = "UI";
		} else if(movimientos.crearCuboDesdePosicion(cuboXF).getPosid() == posIdY) {
			res = "F";
		} else if(movimientos.crearCuboDesdePosicion(cuboXFi).getPosid() == posIdY) {
			res = "FI";
		} else if(movimientos.crearCuboDesdePosicion(cuboXR).getPosid() == posIdY) {
			res = "R";
		} else if(movimientos.crearCuboDesdePosicion(cuboXRi).getPosid() == posIdY) {
			res = "RI";
		} 
		return res;
	}

}