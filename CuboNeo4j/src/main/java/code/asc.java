package code;

import java.util.ArrayList;
import java.util.List;

public class asc {

	public static List<Double> yts = new ArrayList<Double>();

	public static void main(String[] args) {
		for (int t = 0; t <= 100; t++) {
			recursivo(t);
		}
	}

	public static Double recursivo(int t) {
		Double res = 0.0;
		Double yt1 = 0.0;
		Double yt2 = 0.0;
		if (t >= 2) {
			yt1 = yts.get(t - 1);
			yt2 = yts.get(t - 2);
		} else if (t == 1) {
			yt1 = yts.get(t - 1);
			yt2 = 5.0;
		} else if (t == 0) {
			yt1 = 5.0;
			yt2 = 2.0;
		}
		res = 5 * Math.sin(yt1) - 2 * Math.cos(yt2);
		yts.add(res);
		System.out.println(yt2 + " " + yt1 + " " + res);
		return res;
	}
}
