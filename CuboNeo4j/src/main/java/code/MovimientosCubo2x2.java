package code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MovimientosCubo2x2 {

	// Devuelve un Cubo desde una posici�n dada
	public Cubo crearCuboDesdePosicion(List<Integer> posicion) {
		MovimientosCubo2x2 movimientos = new MovimientosCubo2x2();
		List<List<Integer>> posiciones = new ArrayList<List<Integer>>();
		List<Integer> copia = new ArrayList<Integer>(posicion);

		// La parte grande de crear un cubo es sacar sus orientaciones
		// Aqu� sacamos las 24 orientaciones de una posici�n
		// INICIAL
		posiciones.add(copia);

		// RX
		copia = movimientos.rotacionX(copia);
		posiciones.add(copia);

		// RX-RZ
		copia = movimientos.rotacionZ(copia);
		posiciones.add(copia);

		// RX-RYi
		copia = movimientos.rotacionZi(copia);
		copia = movimientos.rotacionYi(copia);
		posiciones.add(copia);

		// RX-RY
		copia = movimientos.rotacionY(copia);
		copia = movimientos.rotacionY(copia);
		posiciones.add(copia);

		// RX-RY-RY
		copia = movimientos.rotacionY(copia);
		posiciones.add(copia);

		// RX-RY-RY-RZ
		copia = movimientos.rotacionZ(copia);
		posiciones.add(copia);

		// RX-RX
		copia = movimientos.rotacionZi(copia);
		copia = movimientos.rotacionYi(copia);
		copia = movimientos.rotacionYi(copia);
		copia = movimientos.rotacionX(copia);
		posiciones.add(copia);

		// RX-RX-RY
		copia = movimientos.rotacionY(copia);
		posiciones.add(copia);

		// RX-RX-RYi
		copia = movimientos.rotacionYi(copia);
		copia = movimientos.rotacionYi(copia);
		posiciones.add(copia);

		// RX-RX-RZ
		copia = movimientos.rotacionY(copia);
		copia = movimientos.rotacionZ(copia);
		posiciones.add(copia);

		// RX-RX-RY-RY
		copia = movimientos.rotacionZi(copia);
		copia = movimientos.rotacionY(copia);
		copia = movimientos.rotacionY(copia);
		posiciones.add(copia);

		// RX-RX-RY-RX
		copia = movimientos.rotacionYi(copia);
		copia = movimientos.rotacionX(copia);
		posiciones.add(copia);

		// RX-RX-RY-RXi
		copia = movimientos.rotacionXi(copia);
		copia = movimientos.rotacionXi(copia);
		posiciones.add(copia);

		// RXi
		copia = new ArrayList<Integer>(posicion);
		copia = movimientos.rotacionXi(copia);
		posiciones.add(copia);

		// RXi-RYi
		copia = movimientos.rotacionYi(copia);
		posiciones.add(copia);

		// RXi-RY
		copia = movimientos.rotacionY(copia);
		copia = movimientos.rotacionY(copia);
		posiciones.add(copia);

		// RXi-RY-RY
		copia = movimientos.rotacionY(copia);
		posiciones.add(copia);

		// RZ
		copia = new ArrayList<Integer>(posicion);
		copia = movimientos.rotacionZ(copia);
		posiciones.add(copia);

		// RZi
		copia = new ArrayList<Integer>(posicion);
		copia = movimientos.rotacionZi(copia);
		posiciones.add(copia);

		// RYi
		copia = new ArrayList<Integer>(posicion);
		copia = movimientos.rotacionYi(copia);
		posiciones.add(copia);

		// RY
		copia = new ArrayList<Integer>(posicion);
		copia = movimientos.rotacionY(copia);
		posiciones.add(copia);

		// RY-RY
		copia = movimientos.rotacionY(copia);
		posiciones.add(copia);

		// RY-RY-RZ
		copia = movimientos.rotacionZ(copia);
		posiciones.add(copia);

		// Creamos el cubo
		Cubo res = new Cubo(posiciones);

		return res;
	}

	
	//FUNCI�N PARA CADA MOVIMIENTO
	public List<Integer> movimientoU(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> arribaCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(16), cubo.get(17), cubo.get(18), cubo.get(19)));
		List<Integer> frontCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(0), cubo.get(1)));
		// FRONT POR RIGHT
		cubo.set(0, cubo.get(4));
		cubo.set(1, cubo.get(5));
		// RIGHT POR BACK
		cubo.set(4, cubo.get(8));
		cubo.set(5, cubo.get(9));
		// BACK POR LEFT
		cubo.set(8, cubo.get(12));
		cubo.set(9, cubo.get(13));
		// LEFT POR FRONT
		cubo.set(12, frontCopia.get(0));
		cubo.set(13, frontCopia.get(1));
		// UP ROTACION
		cubo.set(16, arribaCopia.get(2));
		cubo.set(17, arribaCopia.get(0));
		cubo.set(18, arribaCopia.get(3));
		cubo.set(19, arribaCopia.get(1));
		return cubo;
	}

	public List<Integer> movimientoUi(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> arribaCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(16), cubo.get(17), cubo.get(18), cubo.get(19)));
		List<Integer> leftCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(12), cubo.get(13)));
		// LEFT POR BACK
		cubo.set(12, cubo.get(8));
		cubo.set(13, cubo.get(9));
		// BACK POR RIGHT
		cubo.set(8, cubo.get(4));
		cubo.set(9, cubo.get(5));
		// RIGHT POR FRONT
		cubo.set(4, cubo.get(0));
		cubo.set(5, cubo.get(1));
		// FRONT POR LEFT
		cubo.set(0, leftCopia.get(0));
		cubo.set(1, leftCopia.get(1));
		// UP ROTACION
		cubo.set(16, arribaCopia.get(1));
		cubo.set(17, arribaCopia.get(3));
		cubo.set(18, arribaCopia.get(0));
		cubo.set(19, arribaCopia.get(2));
		return cubo;
	}

	public List<Integer> movimientoD(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> abajoCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(20), cubo.get(21), cubo.get(22), cubo.get(23)));
		List<Integer> leftCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(14), cubo.get(15)));
		// LEFT POR BACK
		cubo.set(14, cubo.get(10));
		cubo.set(15, cubo.get(11));
		// BACK POR RIGHT
		cubo.set(10, cubo.get(6));
		cubo.set(11, cubo.get(7));
		// RIGHT POR FRONT
		cubo.set(6, cubo.get(2));
		cubo.set(7, cubo.get(3));
		// FRONT POR LEFT
		cubo.set(2, leftCopia.get(0));
		cubo.set(3, leftCopia.get(1));
		// DOWN ROTACION
		cubo.set(20, abajoCopia.get(2));
		cubo.set(21, abajoCopia.get(0));
		cubo.set(22, abajoCopia.get(3));
		cubo.set(23, abajoCopia.get(1));
		return cubo;
	}

	public List<Integer> movimientoDi(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> abajoCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(20), cubo.get(21), cubo.get(22), cubo.get(23)));
		List<Integer> frontCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(2), cubo.get(3)));
		// FRONT POR RIGHT
		cubo.set(2, cubo.get(6));
		cubo.set(3, cubo.get(7));
		// RIGHT POR BACK
		cubo.set(6, cubo.get(10));
		cubo.set(7, cubo.get(11));
		// BACK POR LEFT
		cubo.set(10, cubo.get(14));
		cubo.set(11, cubo.get(15));
		// LEFT POR FRONT
		cubo.set(14, frontCopia.get(0));
		cubo.set(15, frontCopia.get(1));
		// DOWN ROTACION
		cubo.set(20, abajoCopia.get(1));
		cubo.set(21, abajoCopia.get(3));
		cubo.set(22, abajoCopia.get(0));
		cubo.set(23, abajoCopia.get(2));
		return cubo;
	}

	public List<Integer> movimientoF(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> upCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(18), cubo.get(19)));
		List<Integer> frontCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(0), cubo.get(1), cubo.get(2), cubo.get(3)));
		// UP POR LEFT
		cubo.set(18, cubo.get(15));
		cubo.set(19, cubo.get(13));
		// LEFT POR DOWN
		cubo.set(13, cubo.get(20));
		cubo.set(15, cubo.get(21));
		// DOWN POR RIGHT
		cubo.set(20, cubo.get(6));
		cubo.set(21, cubo.get(4));
		// RIGHT POR UP
		cubo.set(4, upCopia.get(0));
		cubo.set(6, upCopia.get(1));
		// FRONT ROTACION
		cubo.set(0, frontCopia.get(2));
		cubo.set(1, frontCopia.get(0));
		cubo.set(2, frontCopia.get(3));
		cubo.set(3, frontCopia.get(1));
		return cubo;
	}

	public List<Integer> movimientoFi(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> rightCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(4), cubo.get(6)));
		List<Integer> frontCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(0), cubo.get(1), cubo.get(2), cubo.get(3)));
		// RIGHT POR DOWN
		cubo.set(4, cubo.get(21));
		cubo.set(6, cubo.get(20));
		// DOWN POR LEFT
		cubo.set(20, cubo.get(13));
		cubo.set(21, cubo.get(15));
		// LEFT POR UP
		cubo.set(13, cubo.get(19));
		cubo.set(15, cubo.get(18));
		// UP POR RIGHT
		cubo.set(18, rightCopia.get(0));
		cubo.set(19, rightCopia.get(1));
		// FRONT ROTACION
		cubo.set(0, frontCopia.get(1));
		cubo.set(1, frontCopia.get(3));
		cubo.set(2, frontCopia.get(0));
		cubo.set(3, frontCopia.get(2));
		return cubo;
	}

	public List<Integer> movimientoB(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> rightCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(5), cubo.get(7)));
		List<Integer> backCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(8), cubo.get(9), cubo.get(10), cubo.get(11)));
		// RIGHT POR DOWN
		cubo.set(5, cubo.get(23));
		cubo.set(7, cubo.get(22));
		// DOWN POR LEFT
		cubo.set(22, cubo.get(12));
		cubo.set(23, cubo.get(14));
		// LEFT POR UP
		cubo.set(12, cubo.get(17));
		cubo.set(14, cubo.get(16));
		// UP POR RIGHT
		cubo.set(16, rightCopia.get(0));
		cubo.set(17, rightCopia.get(1));
		// BACK ROTACION
		cubo.set(8, backCopia.get(2));
		cubo.set(9, backCopia.get(0));
		cubo.set(10, backCopia.get(3));
		cubo.set(11, backCopia.get(1));
		return cubo;
	}

	public List<Integer> movimientoBi(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> upCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(16), cubo.get(17)));
		List<Integer> backCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(8), cubo.get(9), cubo.get(10), cubo.get(11)));
		// UP POR LEFT
		cubo.set(16, cubo.get(14));
		cubo.set(17, cubo.get(12));
		// LEFT POR DOWN
		cubo.set(12, cubo.get(22));
		cubo.set(14, cubo.get(23));
		// DOWN POR RIGHT
		cubo.set(22, cubo.get(7));
		cubo.set(23, cubo.get(5));
		// RIGHT POR UP
		cubo.set(5, upCopia.get(0));
		cubo.set(7, upCopia.get(1));
		// BACK ROTACION
		cubo.set(8, backCopia.get(1));
		cubo.set(9, backCopia.get(3));
		cubo.set(10, backCopia.get(0));
		cubo.set(11, backCopia.get(2));
		return cubo;
	}

	public List<Integer> movimientoL(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> upCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(16), cubo.get(18)));
		List<Integer> leftCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(12), cubo.get(13), cubo.get(14), cubo.get(15)));
		// UP POR BACK
		cubo.set(16, cubo.get(11));
		cubo.set(18, cubo.get(9));
		// BACK POR DOWN
		cubo.set(9, cubo.get(22));
		cubo.set(11, cubo.get(20));
		// DOWN POR FRONT
		cubo.set(20, cubo.get(0));
		cubo.set(22, cubo.get(2));
		// FRONT POR UP
		cubo.set(0, upCopia.get(0));
		cubo.set(2, upCopia.get(1));
		// LEFT ROTACION
		cubo.set(12, leftCopia.get(2));
		cubo.set(13, leftCopia.get(0));
		cubo.set(14, leftCopia.get(3));
		cubo.set(15, leftCopia.get(1));
		return cubo;
	}

	public List<Integer> movimientoLi(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> frontCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(0), cubo.get(2)));
		List<Integer> leftCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(12), cubo.get(13), cubo.get(14), cubo.get(15)));
		// FRONT POR DOWN
		cubo.set(0, cubo.get(20));
		cubo.set(2, cubo.get(22));
		// DOWN POR BACK
		cubo.set(20, cubo.get(11));
		cubo.set(22, cubo.get(9));
		// BACK POR UP
		cubo.set(9, cubo.get(18));
		cubo.set(11, cubo.get(16));
		// UP POR FRONT
		cubo.set(16, frontCopia.get(0));
		cubo.set(18, frontCopia.get(1));
		// LEFT ROTACION
		cubo.set(12, leftCopia.get(1));
		cubo.set(13, leftCopia.get(3));
		cubo.set(14, leftCopia.get(0));
		cubo.set(15, leftCopia.get(2));
		return cubo;
	}

	public List<Integer> movimientoR(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> upCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(17), cubo.get(19)));
		List<Integer> rightCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(4), cubo.get(5), cubo.get(6), cubo.get(7)));
		// UP POR FRONT
		cubo.set(17, cubo.get(1));
		cubo.set(19, cubo.get(3));
		// FRONT POR DOWN
		cubo.set(1, cubo.get(21));
		cubo.set(3, cubo.get(23));
		// DOWN POR BACK
		cubo.set(21, cubo.get(10));
		cubo.set(23, cubo.get(8));
		// BACK POR UP
		cubo.set(8, upCopia.get(1));
		cubo.set(10, upCopia.get(0));
		// RIGHT ROTACION
		cubo.set(4, rightCopia.get(2));
		cubo.set(5, rightCopia.get(0));
		cubo.set(6, rightCopia.get(3));
		cubo.set(7, rightCopia.get(1));
		return cubo;
	}

	public List<Integer> movimientoRi(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> backCopia = new ArrayList<Integer>(Arrays.asList(cubo.get(8), cubo.get(10)));
		List<Integer> rightCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(4), cubo.get(5), cubo.get(6), cubo.get(7)));
		// BACK POR DOWN
		cubo.set(8, cubo.get(23));
		cubo.set(10, cubo.get(21));
		// DOWN POR FRONT
		cubo.set(21, cubo.get(1));
		cubo.set(23, cubo.get(3));
		// FRONT POR UP
		cubo.set(1, cubo.get(17));
		cubo.set(3, cubo.get(19));
		// UP POR BACK
		cubo.set(17, backCopia.get(1));
		cubo.set(19, backCopia.get(0));
		// RIGHT ROTACION
		cubo.set(4, rightCopia.get(1));
		cubo.set(5, rightCopia.get(3));
		cubo.set(6, rightCopia.get(0));
		cubo.set(7, rightCopia.get(2));
		return cubo;
	}

	public List<Integer> rotacionX(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> upCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(16), cubo.get(17), cubo.get(18), cubo.get(19)));
		List<Integer> downCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(20), cubo.get(21), cubo.get(22), cubo.get(23)));
		List<Integer> frontCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(0), cubo.get(1), cubo.get(2), cubo.get(3)));
		// FRONT POR RIGHT
		cubo.set(0, cubo.get(4));
		cubo.set(1, cubo.get(5));
		cubo.set(2, cubo.get(6));
		cubo.set(3, cubo.get(7));
		// RIGHT POR BACK
		cubo.set(4, cubo.get(8));
		cubo.set(5, cubo.get(9));
		cubo.set(6, cubo.get(10));
		cubo.set(7, cubo.get(11));
		// BACK POR LEFT
		cubo.set(8, cubo.get(12));
		cubo.set(9, cubo.get(13));
		cubo.set(10, cubo.get(14));
		cubo.set(11, cubo.get(15));
		// LEFT POR FRONT
		cubo.set(12, frontCopia.get(0));
		cubo.set(13, frontCopia.get(1));
		cubo.set(14, frontCopia.get(2));
		cubo.set(15, frontCopia.get(3));
		// UP ROTACION
		cubo.set(16, upCopia.get(2));
		cubo.set(17, upCopia.get(0));
		cubo.set(18, upCopia.get(3));
		cubo.set(19, upCopia.get(1));
		// DOWN ROTACION
		cubo.set(20, downCopia.get(1));
		cubo.set(21, downCopia.get(3));
		cubo.set(22, downCopia.get(0));
		cubo.set(23, downCopia.get(2));
		return cubo;
	}

	public List<Integer> rotacionXi(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> upCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(16), cubo.get(17), cubo.get(18), cubo.get(19)));
		List<Integer> downCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(20), cubo.get(21), cubo.get(22), cubo.get(23)));
		List<Integer> leftCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(12), cubo.get(13), cubo.get(14), cubo.get(15)));
		// LEFT POR BACK
		cubo.set(12, cubo.get(8));
		cubo.set(13, cubo.get(9));
		cubo.set(14, cubo.get(10));
		cubo.set(15, cubo.get(11));
		// BACK POR RIGHT
		cubo.set(8, cubo.get(4));
		cubo.set(9, cubo.get(5));
		cubo.set(10, cubo.get(6));
		cubo.set(11, cubo.get(7));
		// RIGHT POR FRONT
		cubo.set(4, cubo.get(0));
		cubo.set(5, cubo.get(1));
		cubo.set(6, cubo.get(2));
		cubo.set(7, cubo.get(3));
		// FRONT POR LEFT
		cubo.set(0, leftCopia.get(0));
		cubo.set(1, leftCopia.get(1));
		cubo.set(2, leftCopia.get(2));
		cubo.set(3, leftCopia.get(3));
		// UP ROTACION
		cubo.set(16, upCopia.get(1));
		cubo.set(17, upCopia.get(3));
		cubo.set(18, upCopia.get(0));
		cubo.set(19, upCopia.get(2));
		// DOWN ROTACION
		cubo.set(20, downCopia.get(2));
		cubo.set(21, downCopia.get(0));
		cubo.set(22, downCopia.get(3));
		cubo.set(23, downCopia.get(1));
		return cubo;
	}

	public List<Integer> rotacionY(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> leftCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(12), cubo.get(13), cubo.get(14), cubo.get(15)));
		List<Integer> rightCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(4), cubo.get(5), cubo.get(6), cubo.get(7)));
		List<Integer> frontCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(0), cubo.get(1), cubo.get(2), cubo.get(3)));
		// FRONT POR DOWN
		cubo.set(0, cubo.get(20));
		cubo.set(1, cubo.get(21));
		cubo.set(2, cubo.get(22));
		cubo.set(3, cubo.get(23));
		// DOWN POR BACK
		cubo.set(20, cubo.get(11));
		cubo.set(21, cubo.get(10));
		cubo.set(22, cubo.get(9));
		cubo.set(23, cubo.get(8));
		// BACK POR UP
		cubo.set(8, cubo.get(19));
		cubo.set(9, cubo.get(18));
		cubo.set(10, cubo.get(17));
		cubo.set(11, cubo.get(16));
		// UP POR FRONT
		cubo.set(16, frontCopia.get(0));
		cubo.set(17, frontCopia.get(1));
		cubo.set(18, frontCopia.get(2));
		cubo.set(19, frontCopia.get(3));
		// LEFT ROTACION
		cubo.set(12, leftCopia.get(1));
		cubo.set(13, leftCopia.get(3));
		cubo.set(14, leftCopia.get(0));
		cubo.set(15, leftCopia.get(2));
		// RIGHT ROTACION
		cubo.set(4, rightCopia.get(2));
		cubo.set(5, rightCopia.get(0));
		cubo.set(6, rightCopia.get(3));
		cubo.set(7, rightCopia.get(1));
		return cubo;
	}

	public List<Integer> rotacionYi(List<Integer> cuboI) {
		ArrayList<Integer> cubo = new ArrayList<Integer>(cuboI);
		List<Integer> leftCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(12), cubo.get(13), cubo.get(14), cubo.get(15)));
		List<Integer> rightCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(4), cubo.get(5), cubo.get(6), cubo.get(7)));
		List<Integer> upCopia = new ArrayList<Integer>(
				Arrays.asList(cubo.get(16), cubo.get(17), cubo.get(18), cubo.get(19)));
		// UP POR BACK
		cubo.set(16, cubo.get(11));
		cubo.set(17, cubo.get(10));
		cubo.set(18, cubo.get(9));
		cubo.set(19, cubo.get(8));
		// BACK POR DOWN
		cubo.set(8, cubo.get(23));
		cubo.set(9, cubo.get(22));
		cubo.set(10, cubo.get(21));
		cubo.set(11, cubo.get(20));
		// DOWN POR FRONT
		cubo.set(20, cubo.get(0));
		cubo.set(21, cubo.get(1));
		cubo.set(22, cubo.get(2));
		cubo.set(23, cubo.get(3));
		// FRONT POR UP
		cubo.set(0, upCopia.get(0));
		cubo.set(1, upCopia.get(1));
		cubo.set(2, upCopia.get(2));
		cubo.set(3, upCopia.get(3));
		// LEFT ROTACION
		cubo.set(12, leftCopia.get(2));
		cubo.set(13, leftCopia.get(0));
		cubo.set(14, leftCopia.get(3));
		cubo.set(15, leftCopia.get(1));
		// RIGHT ROTACION
		cubo.set(4, rightCopia.get(1));
		cubo.set(5, rightCopia.get(3));
		cubo.set(6, rightCopia.get(0));
		cubo.set(7, rightCopia.get(2));
		return cubo;
	}

	public List<Integer> rotacionZ(List<Integer> cuboI) {
		List<Integer> cubo = new ArrayList<Integer>(cuboI);
		cubo = movimientoF(cubo);
		cubo = movimientoBi(cubo);
		return cubo;
	}

	public List<Integer> rotacionZi(List<Integer> cuboI) {
		List<Integer> cubo = new ArrayList<Integer>(cuboI);
		cubo = movimientoFi(cubo);
		cubo = movimientoB(cubo);
		return cubo;
	}

}