package executable;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import code.Cubo;
import code.MovimientosCubo2x2;

public class EscribirCSV {

	public static void main(String[] args) throws IOException {

		// Creamos las variables
		// Movimientos
		MovimientosCubo2x2 movimientos = new MovimientosCubo2x2();
		// Cubo Inicial
		List<Integer> cuboValido = new ArrayList<Integer>(
				Arrays.asList(1, 1, 1, 1, 4, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 2, 0, 0, 0, 0, 5, 5, 5, 5));

		// Variables de lectura y escritura
		FileWriter ficheroNodes = null;
		PrintWriter printNode = null;
		FileWriter ficheroEdges = null;
		PrintWriter printEdge = null;

		try {
			// Rutas
			ficheroNodes = new FileWriter("C:/Users/Kate/Desktop/nodes.txt");
			ficheroEdges = new FileWriter("C:/Users/Kate/Desktop/edges.txt");
			printNode = new PrintWriter(ficheroNodes);
			printEdge = new PrintWriter(ficheroEdges);

			// Cabecera CSV
			printNode.println("pos");
			printEdge.println("x,y");

			// Contador
			Integer i = 0;
			// Cubo auxiliar (formato List<Integer>)
			List<Integer> cuboAux = new ArrayList<Integer>();
			// Cubo en proceso (formato List<Integer>)
			List<Integer> cuboEnProceso = new ArrayList<Integer>();
			// Cubos analizados (formato String)
			Set<String> cubosAnalizados = new HashSet<String>();
			// Cubos por analizar (formato List<Integer>)
			Set<List<Integer>> cubosParaAnalizar = new HashSet<List<Integer>>();
			// Cubo en proceso (formato Cubo)
			Cubo cuboEnProcesoFormat;
			// Cubo auxiliar (formato Cubo)
			Cubo cuboAuxFormat;

			// A�adimos el cubo inicial al conjunto de cubos por analizar
			cubosParaAnalizar.add(cuboValido);

//			Posibles posiciones: 3674160

			// Mientras queden cubos por analizar
			while (!cubosParaAnalizar.isEmpty()) {
				// Cogemos el primer cubo que analizar
				cuboEnProceso = cubosParaAnalizar.stream().findFirst().get();
				// Le damos formato Cubo
				cuboEnProcesoFormat = movimientos.crearCuboDesdePosicion(cuboEnProceso);

				// Si este no se ha analizado a�n, lo pinto en el archivo
				if (!cubosAnalizados.contains(cuboEnProcesoFormat.getPosid())) {
					printNode.println(cuboEnProcesoFormat.getPosid());

					// PARA CADA MOVIMIENTO (En este caso solo 6 de 12 ya que algunos devuelven el
					// mismo posid (ejemplo:F y B)):
					// Hago el movimiento -> miro si el posid de ese cubo se ha analizado -> si no
					// se ha analizado a�n: lo a�ado a conjunto por analizar y pinto la arista en
					// archivo (posId(cuboEnProceso) -> posId(cuboAuxiliar)

					cuboAux = movimientos.movimientoB(cuboEnProceso);
					cuboAuxFormat = movimientos.crearCuboDesdePosicion(cuboAux);
					if (!(cubosAnalizados.contains(cuboAuxFormat.getPosid()))) {
						cubosParaAnalizar.add(cuboAux);
						printEdge.println('"' + cuboEnProcesoFormat.getPosid() + '"' + "," + '"' +  cuboAuxFormat.getPosid() + '"');
					}

					cuboAux = movimientos.movimientoBi(cuboEnProceso);
					cuboAuxFormat = movimientos.crearCuboDesdePosicion(cuboAux);
					if (!(cubosAnalizados.contains(cuboAuxFormat.getPosid()))) {
						cubosParaAnalizar.add(cuboAux);
						printEdge.println('"' + cuboEnProcesoFormat.getPosid() + '"' + "," + '"' +  cuboAuxFormat.getPosid() + '"');
					}

					cuboAux = movimientos.movimientoD(cuboEnProceso);
					cuboAuxFormat = movimientos.crearCuboDesdePosicion(cuboAux);
					if (!(cubosAnalizados.contains(cuboAuxFormat.getPosid()))) {
						cubosParaAnalizar.add(cuboAux);
						printEdge.println('"' + cuboEnProcesoFormat.getPosid() + '"' + "," + '"' +  cuboAuxFormat.getPosid() + '"');
					}

					cuboAux = movimientos.movimientoDi(cuboEnProceso);
					cuboAuxFormat = movimientos.crearCuboDesdePosicion(cuboAux);
					if (!(cubosAnalizados.contains(cuboAuxFormat.getPosid()))) {
						cubosParaAnalizar.add(cuboAux);
						printEdge.println('"' + cuboEnProcesoFormat.getPosid() + '"' + "," + '"' +  cuboAuxFormat.getPosid() + '"');
					}
					cuboAux = movimientos.movimientoL(cuboEnProceso);
					cuboAuxFormat = movimientos.crearCuboDesdePosicion(cuboAux);
					if (!(cubosAnalizados.contains(cuboAuxFormat.getPosid()))) {
						cubosParaAnalizar.add(cuboAux);
						printEdge.println('"' + cuboEnProcesoFormat.getPosid() + '"' + "," + '"' + cuboAuxFormat.getPosid() + '"');
					}

					cuboAux = movimientos.movimientoLi(cuboEnProceso);
					cuboAuxFormat = movimientos.crearCuboDesdePosicion(cuboAux);
					if (!(cubosAnalizados.contains(cuboAuxFormat.getPosid()))) {
						cubosParaAnalizar.add(cuboAux);
						printEdge.println('"' + cuboEnProcesoFormat.getPosid() + '"' + "," + '"' +  cuboAuxFormat.getPosid() + '"');
					}

					// Incremento el contador
					i++;
					// Muestro en consola el estado del algoritmo
					System.out.println(i + "/" + 3674160);

				}

				// A�adimos la posId del cubo en proceso a la lista de nodos analizados
				cubosAnalizados.add(cuboEnProcesoFormat.getPosid());
				// Eliminamos de nodos a analizar el cubo en proceso y sus orientaciones (ya que
				// tienen el mismo posid)
				cubosParaAnalizar.removeAll(cuboEnProcesoFormat.getPosicionesFinalesInteger());
			}
			// Mensaje de finalizaci�n
			System.out.println("FINAL");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// Cerramos los archivos txt
				if (null != ficheroNodes && null != ficheroEdges)
					ficheroNodes.close();
				ficheroEdges.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}