package executable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import code.MovimientosCubo2x2;

public class TestCubo {

	//Test para sacar las 24 orientaciones de una posici�n del cubo
	
	public static List<Integer> pass(List<Integer> pos) {
		List<Integer> copia = new ArrayList<Integer>(pos);
		pos.set(0, copia.get(6));
		pos.set(1, copia.get(7));
		pos.set(2, copia.get(14));
		pos.set(3, copia.get(15));
		pos.set(4, copia.get(8));
		pos.set(5, copia.get(9));
		pos.set(6, copia.get(16));
		pos.set(7, copia.get(17));
		pos.set(8, copia.get(10));
		pos.set(9, copia.get(11));
		pos.set(10, copia.get(18));
		pos.set(11, copia.get(19));
		pos.set(12, copia.get(4));
		pos.set(13, copia.get(5));
		pos.set(14, copia.get(12));
		pos.set(15, copia.get(13));
		pos.set(16, copia.get(0));
		pos.set(17, copia.get(1));
		pos.set(18, copia.get(2));
		pos.set(19, copia.get(3));
		pos.set(20, copia.get(20));
		pos.set(21, copia.get(21));
		pos.set(22, copia.get(22));
		pos.set(23, copia.get(23));
		return pos;
	}

	public static List<Integer> passInvert(List<Integer> pos) {
		List<Integer> copia = new ArrayList<Integer>(pos);
		pos.set(0, copia.get(16));
		pos.set(1, copia.get(17));
		pos.set(2, copia.get(18));
		pos.set(3, copia.get(19));
		pos.set(4, copia.get(12));
		pos.set(5, copia.get(13));
		pos.set(6, copia.get(0));
		pos.set(7, copia.get(1));
		pos.set(8, copia.get(4));
		pos.set(9, copia.get(5));
		pos.set(10, copia.get(8));
		pos.set(11, copia.get(9));
		pos.set(12, copia.get(14));
		pos.set(13, copia.get(15));
		pos.set(14, copia.get(2));
		pos.set(15, copia.get(3));
		pos.set(16, copia.get(6));
		pos.set(17, copia.get(7));
		pos.set(18, copia.get(10));
		pos.set(19, copia.get(11));
		pos.set(20, copia.get(20));
		pos.set(21, copia.get(21));
		pos.set(22, copia.get(22));
		pos.set(23, copia.get(23));
		return pos;
	}

	public static void main(String[] args) {

		MovimientosCubo2x2 movimientos = new MovimientosCubo2x2();
		List<Integer> cuboValido = new ArrayList<Integer>(
				Arrays.asList(0, 0, 0, 0, 4, 4, 3, 3, 5, 5, 2, 2, 4, 4, 3, 3, 5, 5, 2, 2, 1, 1, 1, 1));

		List<Integer> cuboAux = passInvert(cuboValido);

		System.out.println("INICIAL");
		System.out.println(pass(cuboAux));

		System.out.println("RX");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RX");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RXi");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionXi(cuboValido);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RY");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionY(cuboValido);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RY-RY");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RYi");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionYi(cuboValido);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RY");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RY-RY");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RYi");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionYi(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RX-RY");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionX(cuboAux);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RX-RY-RY");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionX(cuboAux);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RX-RYi");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionX(cuboAux);
		cuboAux = movimientos.rotacionYi(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RXi-RY");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionXi(cuboValido);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RXi-RY-RY");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionXi(cuboValido);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RXi-RYi");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionXi(cuboValido);
		cuboAux = movimientos.rotacionYi(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RXi-RYi");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionXi(cuboValido);
		cuboAux = movimientos.rotacionYi(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RZ");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionZ(cuboValido);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RZ-RZ");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionZ(cuboValido);
		cuboAux = movimientos.rotacionZ(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RZi");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionZi(cuboValido);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RZ");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionZ(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RX-RZ");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionX(cuboAux);
		cuboAux = movimientos.rotacionZ(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RY-RZ");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionY(cuboValido);
		cuboAux = movimientos.rotacionZ(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RY-RY-RZ");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionY(cuboValido);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = movimientos.rotacionZ(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RY-RY-RZ");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = movimientos.rotacionZ(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

		System.out.println("RX-RX-RY-RXi");
		cuboAux = pass(cuboAux);
		cuboAux = movimientos.rotacionX(cuboValido);
		cuboAux = movimientos.rotacionX(cuboAux);
		cuboAux = movimientos.rotacionY(cuboAux);
		cuboAux = movimientos.rotacionXi(cuboAux);
		cuboAux = passInvert(cuboAux);
		System.out.println(cuboAux);

	}

}
