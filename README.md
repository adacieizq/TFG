********************************************RUBIK CUBE 2x2 EXTRACT CSV FOR NEO4J********************************************

La aplicación extrae todas las posibles posiciones del cubo de rubik así como las relaciones entre ellas.

La aplicación consta de las siguientes clases:

- RepresentacionCubo2x2.java --> Esta clase consta de la manera de representar el cubo y está formada por los siguientes métodos:
        
        *Método de comprobación de cubo válido.
        *Método de visualización gráfica del cubo.

- MovimientosCubo2x2.java --> Esta clase contiene las funciones de todos los posibles movimientos que se le puede aplicar a un              cubo de rubik. 
        
        *También contiene una función que nos devuelve todas las posiciones del cubo según su orientación.

- Cubo.java --> Esta clase es la representaciónd del cubo con el formato adecuado para la ejecución del algoritmo.

- TestCubo.kava --> En esta clase ejecutable se llevan a cabo varios tests que comprueban el correcto funcionamiento de los movimientos aplicados a un cubo.

- EscribirCSV.java --> Esta clase ejecutable ejecuta el algoritmo que nos permite obtener todas las posiciones del cubo(nodos) así como la relación entre ellas(aristas) para su posterior importación a Neo4J.

        *Nota: los dos archivos generados (nodes y edges) están en formato txt. Para su correcta importación debemos cambiar el tipo de fichero manualmente antes de importarlo en Neo4J.

Todo el código de este proyecto esta comentado con una breve expxlicación de su funcionamiento.

El proyecto también tiene en la carpeta "ressources" varios archivos de utilidad:
        
        *representacionCubo.jpg --> Imagen con representación gráfica del cubo.
        *CYPHER.txt --> Contiene el código necesario para importar la base de datos en Neo4J.

Link de los archivos generados:
        
        *nodes.csv --> https://drive.google.com/open?id=131Ss6MsJBNCfGe9h69J74gm79_xBDsrn
        *edges.csv --> https://drive.google.com/open?id=1GEAJFNh4jvIXUEk7EPa7I8B7qtUuLPIK
        *graph.db.dump ->  https://drive.google.com/open?id=1MuuuaHfEkkUrhtuPats2ifyOPI_GfGZu